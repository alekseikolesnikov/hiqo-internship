using BowlingKata;
using System;
using Xunit;

namespace BowlingKataTest
{
    public class GameTest
    {
        private const int zeroPinsKnocked = 0;
        private Game game;

        public GameTest()
        {
            game = new Game();
        }

        [Theory]
        [InlineData(0, 20, 0)]
        [InlineData(1, 20, 20)]
        [InlineData(3, 20, 60)]
        public void TestAllTheSamePinsKnockedWithoutExtraRoll(int value, int rolls, int expected)
        {
            RollMany(rolls, value);

            Assert.Equal(expected, game.GetScore());
        }

        [Theory]
        [InlineData(5, 5, 5, 17, 20)]
        [InlineData(6, 4, 5, 17, 20)]
        [InlineData(7, 3, 7, 17, 24)]
        public void TestOneSpare(int roll1, int roll2, int roll3, int rest, int expected)
        {
            game.Roll(roll1);
            game.Roll(roll2);
            game.Roll(roll3);
            RollMany(rest, zeroPinsKnocked);

            Assert.Equal(expected, game.GetScore());
        }

        [Theory]
        [InlineData(10, 5, 5, 16, 30)]
        [InlineData(10, 4, 5, 16, 28)]
        [InlineData(10, 1, 2, 16, 16)]
        public void TestOneStrike(int strike, int roll2, int roll3, int rest, int expected)
        {
            game.Roll(strike);
            game.Roll(roll2);
            game.Roll(roll3);
            RollMany(rest, zeroPinsKnocked);

            Assert.Equal(expected, game.GetScore());
        }


        [Theory]
        [InlineData(10, 10, 5, 4, 4, 4, 13, 61)]
        [InlineData(10, 10, 3, 3, 0, 0, 13, 45)]
        [InlineData(10, 10, 0, 2, 0, 0, 13, 34)]
        public void TestDoubleStrike(int strike1, int strike2, int roll3, int roll4, int roll5, int roll6, int rest, int expected)
        {
            game.Roll(strike1);
            game.Roll(strike2);
            game.Roll(roll3);
            game.Roll(roll4);
            game.Roll(roll5);
            game.Roll(roll6);

            RollMany(rest, zeroPinsKnocked);

            Assert.Equal(expected, game.GetScore());
        }

        [Theory]
        [ClassData(typeof(GameTestData))]
        public void TestTripleStrike(int strike1, int strike2, int strike3, int roll4, int roll5, int rest, int expected)
        {
            game.Roll(strike1);
            game.Roll(strike2);
            game.Roll(strike3);
            game.Roll(roll4);
            game.Roll(roll5);
            RollMany(rest, zeroPinsKnocked);

            Assert.Equal(expected, game.GetScore());
        }

        [Theory]
        [InlineData(21, 5, 150)]
        public void TestAllSpares(int rolls, int points, int expected)
        {
            RollMany(rolls, points);

            Assert.Equal(expected, game.GetScore());
        }

        [Theory]
        [InlineData(18, 5, 2, 4, 142)]
        public void TestLastFrameWithoutExtra(int rolls, int points, int lastrolls, int points2, int expected)
        {
            RollMany(rolls, points);
            RollMany(lastrolls, points2);

            Assert.Equal(expected, game.GetScore());
        }

        [Theory]
        [InlineData(5, 4, 1)]
        public void TestCommonTransitionToNextRoll(int roll1, int roll2, int expected)
        {
            game.Roll(roll1);
            game.Roll(roll2);
            var actual = game.Rolls.FindIndex(x => x == 4);

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(4, 6, 7, 2)]
        public void TestSpareTransitionToNextRoll(int roll1, int roll2, int roll3, int expected)
        {
            game.Roll(roll1);
            game.Roll(roll2);
            game.Roll(roll3);
            var actual = game.Rolls.FindIndex(x => x == 7);

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(10, 6, 2)]
        public void TestStrikeTransitionToNextRoll_ReturnsIndexOfNext(int roll1, int roll2, int expected)
        {
            game.Roll(roll1);
            game.Roll(roll2);
            var actual = game.Rolls.FindIndex(x => x == 6);

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(10, 6, 1)]
        public void TestStrikeTransitionToNextRoll_ReturnsIndexOfEmpty(int roll1, int roll2, int expected)
        {
            game.Roll(roll1);
            game.Roll(roll2);
            var actual = game.Rolls.FindIndex(x => x == 0);

            Assert.Equal(expected, actual);
        }

        private void RollMany(int rolls, int pins)
        {
            for (int i = 0; i < rolls; i++)
            {
                game.Roll(pins);
            }
        }
    }
}
