﻿using System.Collections;
using System.Collections.Generic;

namespace BowlingKataTest
{
    public class GameTestData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { 10, 10, 10, 4, 4, 13, 80 };
            yield return new object[] { 10, 10, 10, 4, 0, 13, 72 };
            yield return new object[] { 10, 10, 10, 0, 0, 13, 60 };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
