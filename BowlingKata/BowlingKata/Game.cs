﻿using System.Collections.Generic;

namespace BowlingKata
{
    public class Game
    {
        private const int startValue = 0;
        private const int maxRollCount = 21;
        private const int step = 2;
        private const int allPinsKnockedScore = 10;

        private int score;
        private int currentRoll;

        public List<int> Rolls = new List<int>(maxRollCount);
        public Game()
        {
            score = startValue;
            currentRoll = startValue;

            for (int i = 0; i < maxRollCount; i++)
                Rolls.Add(startValue);
        }

        public void Roll(int pins)
        {
            Rolls[currentRoll] = pins;

            if (IsStrike(currentRoll) && !IsLastFrame(currentRoll))
                ToNextFrame();
            else
                ToNextRoll();
        }

        public int GetScore()
        {
            int roll = startValue;

            while (!IsLastFrame(roll))
            {
                if (IsStrike(roll))
                    score += StrikeBonus(roll);
                else if (IsSpare(roll))
                    score += SpareBonus(roll);
                else
                    score += CommonFrameSum(roll);

                roll += step;
            }

            if (IsLastFrame(roll))
            {
                if (IsMeritExtraRoll(roll))
                    score += SumWithExtra(roll);
                else
                    score += CommonFrameSum(roll);
            }

            return score;
        }

        private int StrikeBonus(int roll)
        {
            if (!IsLastFrame(roll))
            {
                if (IsDoubleStrike(roll))
                {
                    if (IsTripleStrike(roll))
                        return 30;

                    return 20 + Rolls[roll + 4];
                }

                return allPinsKnockedScore + Rolls[roll + 2] + Rolls[roll + 3];
            }
            else
                return allPinsKnockedScore;
        }

        private int SpareBonus(int roll)
        {
            if (!IsLastFrame(roll))
                return allPinsKnockedScore + Rolls[roll + 2];
            else
                return allPinsKnockedScore;
        }

        private int CommonFrameSum(int roll)
        {
            return Rolls[roll] + Rolls[roll + 1];
        }

        private int SumWithExtra(int roll)
        {
            return Rolls[roll] + Rolls[roll + 1] + Rolls[roll + 2];
        }

        private bool IsSpare(int roll)
        {
            return (Rolls[roll] + Rolls[roll + 1]) == allPinsKnockedScore;
        }

        private bool IsStrike(int roll)
        {
            return Rolls[roll] == allPinsKnockedScore;
        }

        private bool IsDoubleStrike(int roll)
        {
            return IsStrike(roll) && IsStrike(roll + 2);
        }

        private bool IsTripleStrike(int roll)
        {
            return IsDoubleStrike(roll) && IsStrike(roll + 4);
        }

        private bool IsMeritExtraRoll(int roll)
        {
            if (IsLastFrame(roll))
                return IsSpare(roll) || IsStrike(roll);
            else
                return IsStrike(roll);
        }

        private bool IsLastFrame(int roll)
        {
            return roll > 17;
        }

        private void ToNextFrame()
        {
            currentRoll++;
            currentRoll++;
        }

        private void ToNextRoll()
        {
            currentRoll++;
        }
    }
}
