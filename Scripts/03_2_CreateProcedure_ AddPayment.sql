﻿USE [Store]
GO

IF OBJECT_ID('usp_AddPayment') IS NOT NULL
    DROP PROCEDURE usp_AddPayment
GO

CREATE PROCEDURE usp_AddPayment
	@user_Id INT,
	@price DECIMAL(6,2)
AS
BEGIN
	INSERT INTO Payments VALUES (@user_Id, @price)
END