﻿USE [Store]
GO

IF OBJECT_ID('usp_ReadPayment_Id') IS NOT NULL
	DROP PROCEDURE usp_ReadPayment_Id
GO

CREATE PROCEDURE usp_ReadPayment_Id
	@payment_id INT
AS
BEGIN
	SELECT * FROM Payments p
	WHERE p.Id = @payment_id
END