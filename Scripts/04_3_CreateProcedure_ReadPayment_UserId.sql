﻿USE [Store]
GO

IF OBJECT_ID('usp_ReadPayment_UserId') IS NOT NULL
	DROP PROCEDURE usp_ReadPayment_UserId
GO

CREATE PROCEDURE usp_ReadPayment_UserId
	@user_id INT
AS
BEGIN
	SELECT * FROM Payments p 
	WHERE p.UserId = @user_id
END