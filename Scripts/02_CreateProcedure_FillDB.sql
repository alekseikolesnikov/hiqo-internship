﻿USE[Store]
GO

IF OBJECT_ID('usp_Fill_UsersPayments_Random') IS NOT NULL
    DROP PROCEDURE usp_Fill_UsersPayments_Random
GO

CREATE PROCEDURE usp_Fill_UsersPayments_Random
	@users_to_insert INT,
	@min_payment_count INT,
	@max_payment_count INT
AS
DECLARE	
	@number_Of_payments INT,
	@price DECIMAL(6,2) = 0,
	@users_count INT = (SELECT COUNT(Id) FROM Users),
	@users_left INT;
BEGIN
	SET @users_left =  @users_count + @users_to_insert;

	WHILE @users_count < @users_left
	BEGIN
		INSERT INTO Users VALUES ('Fname','Lname','Mname');
		SET @number_Of_payments = (ABS(CHECKSUM(NEWID())) % (@max_payment_count - @min_payment_count + 1) + @min_payment_count);
		WHILE @number_Of_payments - @min_payment_count < @max_payment_count
			BEGIN
				SET @price = CAST((RAND() * 1000) as DECIMAL(6,2));
				INSERT INTO Payments VALUES (@@IDENTITY, @price);
				SET @number_Of_payments = @number_Of_payments + 1;
			END
			SET @users_left = @users_left - 1;
	END
END

GO