﻿USE Store
GO

IF OBJECT_ID('usp_SearchUser_FullMatch') IS NOT NULL
	DROP PROCEDURE usp_SearchUser_FullMatch
GO

CREATE PROCEDURE usp_SearchUser_FullMatch
	@first_name NVARCHAR(50),
	@last_name NVARCHAR(50),
	@middle_name NVARCHAR(50)
AS
BEGIN
	SELECT * FROM Users
	WHERE FirstName = @first_name AND LastName = @last_name AND MiddleName = @middle_name;
END