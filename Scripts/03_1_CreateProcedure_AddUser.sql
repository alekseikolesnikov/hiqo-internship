﻿USE[Store]
GO

IF OBJECT_ID('usp_AddUser') IS NOT NULL
    DROP PROCEDURE usp_AddUser
GO

CREATE PROCEDURE usp_AddUser
	@first_name NVARCHAR(50),
	@last_name NVARCHAR(50),
	@middle_name NVARCHAR(50)
AS
BEGIN
	INSERT INTO Users VALUES (@first_name, @last_name, @middle_name);
END