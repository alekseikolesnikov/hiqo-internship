﻿USE [Store]
GO

IF OBJECT_ID('usp_ReadUser') IS NOT NULL
    DROP PROCEDURE usp_ReadUser
GO

CREATE PROCEDURE usp_ReadUser
	@user_id INT
AS
BEGIN
	SELECT * FROM Users u 
	WHERE u.Id = @user_id
END