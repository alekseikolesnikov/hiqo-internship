﻿USE Store
GO

IF OBJECT_ID('usp_SearchUser_PartitialMatch') IS NOT NULL
	DROP PROCEDURE usp_SearchUser_PartitialMatch
GO

CREATE PROCEDURE usp_SearchUser_PartitialMatch
	@first_name NVARCHAR(50),
	@last_name NVARCHAR(50),
	@middle_name NVARCHAR(50)
AS
BEGIN
	SELECT @first_name ='%' + RTRIM(@first_name) + '%';
	SELECT @last_name ='%' +  RTRIM(@last_name) + '%';
	SELECT @middle_name ='%' + RTRIM(@middle_name) + '%';

	SELECT * FROM Users
	WHERE FirstName LIKE @first_name AND LastName LIKE @last_name AND MiddleName LIKE @middle_name;
END